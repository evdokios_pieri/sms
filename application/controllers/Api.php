<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->config = array(
				'SID' => SID,
				'SECRET' => SECRET
			);

		$this->load->library('Twilio', $this->config);		
		
		
		$this->twilio = new Twilio($this->config);
	}

	
	public function smspromotion()
	{
		$data = $this->input->post();

		if ( is_numeric($data['phone']) && strlen($data["phone"]) == 12 )
		{
			$status = $this->twilio->sendSms($data['phone'], $this->statusMessage());
		}	
		else{
			echo json_encode("Phone Number should contain only numbers, such as +35799123456");
		}
		
	}

	private function statusMessage()
	{
		if (date("H") < 12)
		{
		    $message = 'Good morning! Your promocode is AM123';
		} else if (date('H') > 11) 
		{
		    $message = 'Hello! Your promocode is PM456';
		}

		return $message;
	}
}
