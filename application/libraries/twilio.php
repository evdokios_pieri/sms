<?php 

require_once('vendor/autoload.php');
use Twilio\Rest\Client;

class Twilio
{
	public function __construct($config = array())
    {
    	$this->client = new Client($config['SID'], $config['SECRET']);
    	$this->from = PHONENUMBER;
    }

    public function sendSms($to = "", $body = "")
    {
    	return $this->client->messages->create( $to, array('from' => $this->from, 'body' => $body) );
    }
}


