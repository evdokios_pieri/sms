Instructions:

Front End uses ajax to send requests to the Api Controller.

Twilio Library: application/libraries/Twilio.php

Api controller: application/controllers/Api.php

Front End: application/controllers/Home.

Ajax is found: assets/js/main.js

Twilio Settings: application/config/constants

You can run the project if you setup it locally:
www.sms.com/index.php/Home